import glob
import os
import shutil
import socket
import time

from ratelimit import limits, sleep_and_retry

from tqdm import tqdm

"""

Send bulk of log-files to the remote Syslog-server
--------------------------------------------------
Copyright 2019 Miikka (https://gitlab.com/miikkav)


What is this?
-------------
This simple Python app that reads all the files with defined prefix (.log in default) 
that are located in the same folder that it is executed and then sends them via UDP to 
the defined destination ip-address and port. After processing the file it moves it to 
the folder that is defined in the 'processed_dir'-variable. 

This application uses only UDP-protocol for sending at the moment, because:

1. When sending large amounts of logs it is somewhat faster (but more unreliable) than TCP

2. RFC5426 is clear, with "Each syslog UDP datagram MUST contain only one syslog 
message, which MAY be complete or truncated." statement, which simplifies things


What is it for?
---------------
Purpose of this program is to resend bulk of logs to the destination with 
capability to rate-limiting. 

"""

# Set variables
lines_read = 0
counter = 0
eps = 20000
files = '*.log'
destination_ip = '127.0.0.1'
destination_port = 514
processed_dir = 'processed'
syslog_messages = []

# Create dir for processed files, if not already created
if not os.path.isdir(processed_dir):
    os.mkdir(processed_dir)

# Define functions

# Open UDP-connection
def open_udp_connection():
    """ This function initialises the connection """
    global destination_ip
    global destination_port
    global s
    dst = (str(destination_ip), destination_port)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(dst)

# Function to send log-line, with rate limiting instated
@sleep_and_retry
@limits(calls=eps, period=1)
def send_row(line):
    """ This function does the UDP-sending to the destination, with rate-limiting """
    global s
    global counter
    line_to = line
    s.send(line_to)
    counter += 1
    pbar.update()

def statistics():
    """ This function prints statistics about program when called """
    global lines_read
    global counter
    global total_eps
    global syslog_files
    print("")
    print('And we are done! Here are some statistics:')
    print('=========================================')
    print("Files read: " + str(len(syslog_files)))
    print("Lines written from files to a list: " + str(lines_read))
    print("Lines sended from that list towards destination: " + str(counter))
    print("And to do it all it took " + str(time) + " seconds, which makes " + str(total_eps) + " EPS")


# Read filenames to the list
syslog_files = [f for f in glob.glob(files)]

# Init UDP-connection
open_udp_connection()

# Start timer
start = time.time()

# Start processing files
for filename in syslog_files:
    processed_path = 'processed/' + filename

# Read contents of a file to a list
    with open(filename, "r") as opened_file:

        for line in opened_file:
            syslog_messages.append(line.strip('\n'))
            lines_read += 1
        pbar = tqdm(total=lines_read, desc="Sending logs: " + str(lines_read),unit="e")

        # Send contents of a list via UDP
# Repeat until list empty, in case there are duplicates
        while len(syslog_messages) != 0:
            for line in syslog_messages:
                line_to_send = syslog_messages.pop()
                send_row(line_to_send)

# Move processed files to new directory
        shutil.move(filename,processed_path)

pbar.close()
# Stop Timer
end = time.time()

# Count time and EPS-rate
time = end - start
total_eps = counter / time


# Print statistics regarding program
statistics()