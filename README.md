Send bulk of log-files to the remote Syslog-server
--------------------------------------------------


What is this?
-------------
This simple Python app that reads all the files with defined prefix (.log in default) 
that are located in the same folder that it is executed and then sends them via UDP to 
the defined destination ip-address and port. After processing the file it moves it to 
the folder that is defined in the 'processed_dir'-variable. 

This application uses only UDP-protocol for sending at the moment, because:

1. When sending large amounts of logs it is somewhat faster (but more unreliable) than TCP

2. RFC5426 is clear with "Each syslog UDP datagram MUST contain only one syslog 
message, which MAY be complete or truncated." statement, which simplifies things.


What is it for?
---------------
Purpose of this app is to be able to resend bulk of logfiles with UDP to the remote syslog-server with 
capability to do rate-limiting.

Usage
-----

* Install requirements with ``pip install -f requirements.txt``
* Drag log files to the folder ``sender.py`` is located (or move ``sender.py``)
* Do required modifications to ``sender.py`` variables
* Run ``python sender.py``


Tested with Python 3.6 and in virtualenv.
